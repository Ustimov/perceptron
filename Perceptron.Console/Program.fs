﻿open Perceptron
open System.IO
open System.Drawing
open System

let printNumber i = 
    let mutable counter = 0
    for j in i do
        if j = 1.0 then
            printf "0"
        else
            printf "-"
        counter <- counter + 1
        if counter = 5 then
            counter <- 0
            printfn ""
    printfn ""

let loadInputFrom path =
    let files = Directory.GetFiles(path)
    let input = ResizeArray<float[]>();
    let digit = ResizeArray<int>()
    let black = Color.FromArgb(255, 0, 0, 0)
    for file in files do
        use bitmap = new Bitmap(file)
        let array: float[] = Array.zeroCreate (bitmap.Height * bitmap.Width)
        for i in 0..bitmap.Height - 1 do
            for j in 0..bitmap.Width - 1 do
                array.[j + i * bitmap.Width] <-
                    if bitmap.GetPixel(j, i) = black then 1.0 else 0.0
        input.Add(array)
        digit.Add(int(Path.GetFileName(file).Substring(0, 1)))
    input, digit

let train (perceptrons: Perceptron[]) input digit alpha maxEpochs = 
    let trainData = input
                    |> Seq.toArray
    for i in 0..perceptrons.Length - 1 do
        let desired = digit
                      |> Seq.map (fun d -> if d = (i + 1) then 1.0 else -1.0)
                      |> Seq.toArray
        perceptrons.[i].Train(trainData, desired, alpha, maxEpochs)
        |> ignore

[<EntryPoint>]
let main argv = 
    let input, digit = loadInputFrom "../../../Images/train"

    let alpha = 0.001
    let maxEpochs = 100

    let perceptrons: Perceptron[] = Array.zeroCreate 10
    for i in 0..perceptrons.Length - 1 do
        perceptrons.[i] <- Perceptron(35)

    train perceptrons input digit alpha maxEpochs

    let input, _ = loadInputFrom "../../../Images/test7"

    for perceptron in perceptrons do
        printf "%d: " (Array.IndexOf(perceptrons, perceptron) + 1)
        let results = input
                      |> Seq.map (fun i -> perceptron.ComputeOutput(i))
                      |> Seq.toArray
        let recognizedCount = results
                              |> Seq.fold (fun c r -> if r = 1.0 then c + 1 else c) 0
        printfn "[%d] %A" recognizedCount results

    0
