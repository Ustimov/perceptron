﻿namespace App.ViewModel

open System
open System.Collections.ObjectModel
open System.Windows
open System.Windows.Data
open System.Windows.Forms
open System.Windows.Input
open System.ComponentModel
open System.IO
open System.Drawing
open App.Model
open System.Windows.Controls

type MainWindowViewModel() =
    inherit ViewModelBase()

    let width = 5
    let height = 7
    let mutable selectedIndex = -1

    let mutable frame: Option<Frame> = None
    let mutable segmentedControlViewModel: Option<SegmentedControlViewModel> = None

    let inputImages = ObservableCollection<InputImage>()

    let saveImage(file: string) = 
        if file.EndsWith(".png") then
            use bitmap = new Bitmap(file)
            if bitmap.Width = width && bitmap.Height = height then
                let pixels = Array.zeroCreate (width * height)
                for i in 0..height - 1 do
                    for j in 0..width - 1 do
                        let pixel = bitmap.GetPixel(j, i)
                        if pixel = Color.Black then
                            pixels.[j + (width * i)] <- 1.0
                        else
                            pixels.[j + (width * i)] <- 0.0
                        inputImages.Add({ Name = Path.GetFileName(file); Value = pixels })

    let openImages() = 
        use folderBrowserDialog = new FolderBrowserDialog()
        if folderBrowserDialog.ShowDialog() = DialogResult.OK then
            let path = folderBrowserDialog.SelectedPath
            let files = Directory.GetFiles(path)
            for file in files do
                saveImage(file)

    let saveFrame (f: Frame) = 
        let viewModel = SegmentedControlViewModel()
        let content: FrameworkElement = f.Content :?> FrameworkElement
        content.DataContext <- segmentedControlViewModel
        segmentedControlViewModel <- Some(viewModel)
        frame <- Some(f)

    let rebindSegmentedControlViewModel() =
        match segmentedControlViewModel with
        | None -> ()
        | Some(viewModel) ->
            match frame with
            | None -> ()
            | Some(f) ->
                let content: FrameworkElement = f.Content :?> FrameworkElement
                content.DataContext <- viewModel
                        
    member this.OpenCommand = 
        RelayCommand ((fun _ -> true), (fun _ -> openImages()))

    member this.FrameCommand = 
        RelayCommand ((fun _ -> true), (fun f -> saveFrame (f :?> Frame)))

    member this.InputImages = inputImages

    member this.SelectedIndex
        with get() = selectedIndex
        and set value =
            selectedIndex <- value
            match segmentedControlViewModel with
            | Some(viewModel) when selectedIndex > 0 ->
                viewModel.SetImage(this.InputImages.[selectedIndex].Value)
                rebindSegmentedControlViewModel()
            | _ -> ()