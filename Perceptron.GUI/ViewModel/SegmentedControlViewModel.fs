﻿namespace App.ViewModel

open System.Collections.ObjectModel
open System.Windows.Media

type SegmentedControlViewModel() =
    inherit ViewModelBase()

    let black = SolidColorBrush(ColorConverter.ConvertFromString("Black") :?> Color)
    let white = SolidColorBrush(ColorConverter.ConvertFromString("White") :?> Color)

    member this.Segments = ObservableCollection<Brush>()

    member this.SetImage(points: float[]) =
        let mutable counter = 0
        this.Segments.Clear()
        for point in points do
            if point = 1.0 then
                this.Segments.Add(black)
            else
                this.Segments.Add(white)
            this.OnPropertyChanged (sprintf "Segments[%d]" counter)
            counter <- counter + 1