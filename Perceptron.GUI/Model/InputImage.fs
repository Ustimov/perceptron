﻿namespace App.Model

type InputImage =
    { Name : string
      Value : float[] }
