﻿namespace Perceptron

open System

type Perceptron(numberOfInputs) = 
    let weights: float[] = Array.zeroCreate numberOfInputs
    let mutable bias: float = 0.0
    let random = Random(0)

    let shuffle(array: int[]) = 
        for i in 0..array.Length - 1 do
            let r = random.Next(i, array.Length)
            let tmp = array.[r]
            array.[r] <- array.[i]
            array.[i] <- tmp
        array

    let initializeWeights() =
        let lo = -0.01
        let hi = 0.01
        for i in 0..weights.Length - 1 do
            weights.[i] <- (hi - lo) / random.NextDouble() + lo
        bias <- (hi - lo) / random.NextDouble() + lo

    let activate(sum: float) = 
        if sum >= 0.0 then 1.0 else -1.0

    let update(inputs: float[], computed: float, desired: float, alpha: float) = 
        if not (computed = desired) then
            let delta = computed - desired
            for i in 0..weights.Length - 1 do
                if inputs.[i] >= 0.0 then
                    weights.[i] <- weights.[i] - (alpha * delta * inputs.[i])
                elif inputs.[i] < 0.0 then
                    weights.[i] <- weights.[i] + (alpha * delta * inputs.[i])
            bias <- bias - (alpha * delta)

    member this.Train(trainData: float[][], desired: float[], alpha: float, maxEpochs: int) = 
        let sequence = [for i in 0..trainData.Length - 1 -> i] |> Seq.toArray |> shuffle
        for _ in 1..maxEpochs do
            for i in sequence do
                let result = this.ComputeOutput(trainData.[i])
                update(trainData.[i], result, desired.[i], alpha)
        weights, bias
        
    member this.ComputeOutput(inputs: float[]) = 
        let mutable sum = 0.0
        for i in 0..weights.Length - 1 do
            sum <- sum + (inputs.[i] * weights.[i])
        sum <- sum + bias
        activate(sum)
 